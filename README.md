# Spike Report

## C++ Basics

### Introduction

We wish to use Unreal Engine’s C++ to code throughout the semester, but we do not have a solid understanding. We have experience in a number of programming languages, but little understanding of C++. Unreal Engine’s C++ can be easier to learn than standalone C++, but we should still understand the language before continuing.

### Goals

1. A simple numeric guessing game on the Command Line/Console, written in C++
	1. A random number between 0-99 (inclusive) is selected by the program to be guessed by the user
	1. The user is prompted to enter a number until they do so 
		* Ignoring non-numeric input, and prompting the user to try again
	1. Once a number has been given, it is checked for correctness
		* If correct, the game ends, and the user is told how many guesses they made
	1. If not correct, the user is told whether the number they are seeking is smaller or larger than the input number

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [C++ Reference](http://www.cplusplus.com/reference/)
* [C++ Seeding Surprises](http://www.pcg-random.org/posts/cpp-seeding-surprises.html)

### Tasks Undertaken

1. Create a new C++ console project in Visual Studio
1. Make use of pre-existing libraries
	1. Use `<iostream>` and `<string>` for input
	1. Use `<random>` for number generation
	1. Use `<regex>` for input validation

### What we found out

The primary focus here is how to handle input to check as an integer, the process of which cannot be safely done using only `std::cin` and a typecast. Notably, `std::cin` will separate input containing things like a space into separate inputs, and will hold on to unused inputs until it is next read, presenting said old input. To avoid this, we can use `std::getline(std::cin, stringVariable)` to collect all of an input as one string. A tool like regular expressions can be used to then filter input to only accept valid numeric input. To briefly touch upon the regular expression used here, `"^-?\\d+$"` will match with a string that begins (`^`) with zero or one "-" signs (`-?`), and is then followed by one or more numbers (`//d`) and the end of the string (`$`). After this, we can safely convert to int for comparisons using `std::stoi`.

```C++
if (std::regex_match(stringVariable, std::regex("^-?\\d+$"))) {
	intGuess = std::stoi(stringVariable, nullptr);
	...
}
```

C++ can also have less than ideal random number generation using the regular `std::rand()` and `std::srand()` functions. To avoid this, we can utilise `std::random_device` instead. The benefits here are access to `std::uniform_int_distribution` to ensure equal chances for all numbers being selected, and a random number sequence which will differ each time the program is run, without needing to lean on things like the system time in code.

```C++
std::random_device randDev;
std::uniform_int_distribution<int> dist(guessMin, guessMax);
int magicNumber = dist(randDev);
```

### Open Issues/Risks

1. The only potential known issue at current, is the acceptance of -0 as a valid input. Arguably, this may be reasonable and would be a stylistic decision rather than a true issue.