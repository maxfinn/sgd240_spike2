// sgd240_spike2.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

#include <iostream>
#include <random>
#include <regex>
#include <string>


int main()
{
	//Allow for adjustable ranges
	int guessMin = 0;
	int guessMax = 99;

	//Generate the random number to guess
	std::random_device randDev;
	std::uniform_int_distribution<int> dist(guessMin, guessMax);
	int magicNumber = dist(randDev);

	std::cout << "Hi, I've picked a number from 0 to 99. Take a guess, or maybe many!\n";
	int currentTry = 0;

	while (true) {
		currentTry++;
		std::string numGuess;
		int intGuess;

		//Get a valid input from the player
		//Invalid inputs do not use up a guess
		while (true) {
			std::cout << "Guess " << currentTry << ": ";
			std::getline(std::cin, numGuess);

			//Check if input is a number
			if (std::regex_match(numGuess, std::regex("^-?\\d+$"))) {
				intGuess = std::stoi(numGuess, nullptr);

				//Check that the guess is in range, and remind the player of the guess range if not
				if (intGuess < guessMin || guessMax < intGuess) {
					std::cout << "You do remember that the number was from " << guessMin <<
						" to " << guessMax << ", don't you? Have another go.\n";
				}
				else {
					//Input is valid, proceed to comparing with magicNumber
					break;
				}
			}
			else {
				std::cout << "Hmm, I don't quite think that's a number. Have another go.\n";
			}
		}

		//Guide the player either up or down if required
		if (magicNumber > intGuess) {
			std::cout << "Try something higher.\n";
		}
		else if (magicNumber < intGuess) {
			std::cout << "Try something lower.\n";
		}
		//Win scenario: Notify player and end game
		else if (magicNumber == intGuess) {
			if (currentTry == 1) {
				std::cout << "Remarkable, you got it right on your first try!\n";
			}
			else {
				std::cout << "You got it right, and in only " << currentTry << " tries!\n";
			}

			break;
		}
	}

	return 0;
}

